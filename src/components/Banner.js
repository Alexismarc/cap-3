import { Button, Row, Col } from 'react-bootstrap';

import {Link} from 'react-router-dom';

export default function Banner(){
    return (
        <Row>
            <Col className="p-5 ">
                <h1>Reveil</h1>
                <p>Unlock the Beauty of Smooth, Frizz-Free Hair with ReVeil Brazilian Blowout</p>
                <Button as = {Link} to = '/products' variant="primary">Buy now!</Button>
            </Col>
        </Row>

    )
}