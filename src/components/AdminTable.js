import React, { useState, useEffect } from 'react';
import { Row, Col, Table, Button, Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function AdminTable() {
  const [products, setProducts] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const [editProductId, setEditProductId] = useState(null);
  const [editProductName, setEditProductName] = useState('');
  const [editProductDescription, setEditProductDescription] = useState('');
  const [editProductPrice, setEditProductPrice] = useState('');

  useEffect(() => {
    fetchProducts();
  }, []);

  const fetchProducts = () => {
    fetch(`${process.env.REACT_APP_API_URL}/products/all`)
      .then((response) => response.json())
      .then((data) => setProducts(data));
  };

  const handleEdit = (productId, productName, productDescription, productPrice) => {
    setEditProductId(productId);
    setEditProductName(productName);
    setEditProductDescription(productDescription);
    setEditProductPrice(productPrice);
    setShowModal(true);
  };

  const handleUpdate = () => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${editProductId}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({
        name: editProductName,
        description: editProductDescription,
        price: editProductPrice,
      }),
    })
      .then((response) => response.json())
      .then((data) => {
        if (data) {
          setShowModal(false);
          fetchProducts();
          Swal.fire({
            icon: 'success',
            title: 'Product Updated',
            text: 'Product information has been successfully updated.',
          });
        } else {
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Failed to update product information. Please try again.',
          });
        }
      });
  };

const handleActivateDeactivate = (productId, currentStatus) => {
  const newStatus = !currentStatus;
  const url = newStatus
    ? `${process.env.REACT_APP_API_URL}/products/${productId}/reactivate`
    : `${process.env.REACT_APP_API_URL}/products/${productId}/deactivate`;

  const token = localStorage.getItem('token'); // Fetch the authentication token from localStorage

  fetch(url, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`, // Include the authorization token in the headers
    },
  })
    .then((response) => response.json())
    .then((data) => {
      console.log('API response:', data);

      if (data) {
        fetchProducts();
        Swal.fire({
          icon: 'success',
          title: 'Status Updated',
          text: `Product status has been successfully ${newStatus ? 'activated' : 'deactivated'}.`,
        });
      } else {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: `Failed to ${newStatus ? 'activate' : 'deactivate'} product. Please try again.`,
        });
      }
    });
};



  return (
    <div>
      <Table striped bordered hover responsive>
        <thead>
          <tr>
            <th>Product Name</th>
            <th>Product Description</th>
            <th>Price</th>
            <th>Status</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {products.map((product) => (
            <tr key={product._id}>
              <td>{product.name}</td>
              <td>{product.description}</td>
              <td>₱{product.price}</td>
              <td>{product.isActive ? 'Active' : 'Inactive'}</td>
              <td>
                <Button
                  variant={product.isActive ? 'danger' : 'success'}
                  onClick={() => handleActivateDeactivate(product._id, product.isActive)}
                >
                  {product.isActive ? 'Deactivate' : 'Activate'}
                </Button>{' '}
                <Button variant="primary" onClick={() => handleEdit(product._id, product.name, product.description, product.price)}>
                  Edit
                </Button>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>

      <Modal show={showModal} onHide={() => setShowModal(false)}>
        <Modal.Header closeButton>
          <Modal.Title>Edit Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group controlId="productName">
              <Form.Label>Product Name</Form.Label>
              <Form.Control type="text" value={editProductName} onChange={(e) => setEditProductName(e.target.value)} />
            </Form.Group>
            <Form.Group controlId="productDescription">
              <Form.Label>Product Description</Form.Label>
              <Form.Control
                as="textarea"
                rows={3}
                value={editProductDescription}
                onChange={(e) => setEditProductDescription(e.target.value)}
              />
            </Form.Group>
            <Form.Group controlId="productPrice">
              <Form.Label>Price</Form.Label>
              <Form.Control type="number" value={editProductPrice} onChange={(e) => setEditProductPrice(e.target.value)} />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={() => setShowModal(false)}>
            Close
          </Button>
          <Button variant="primary" onClick={handleUpdate}>
            Update
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
}

