// import {Row, Col, Button, Card} from 'react-bootstrap';
// //the useParams allows us to get or extract the parameter included in our pages
// import { useParams } from 'react-router-dom';

// import {useEffect, useState} from 'react';

// import Swal2 from 'sweetalert2';

// export default function ProductView(){

// 	const [name, setName] = useState('');
// 	const [price, setPrice] = useState('');
// 	const [description, setDescription] = useState('');


// 	const {id} = useParams();
// 	/*console.log(id);*/

// 	useEffect(()=> {
// 		fetch(`${process.env.REACT_APP_API_URL}/products/${id}`)
// 		.then(response => response.json())
// 		.then(data => {
// 			// console.log(data);
// 				setName(data.name);
// 				setPrice(data.price);
// 				setDescription(data.description);

// 		})



// 	}, [])

// 	const checkout = (productId) => {
// 		fetch(`${process.env.REACT_APP_API_URL}/users/checkout`,{
// 			method: "POST",
// 			headers: {
// 				'Content-Type' : 'application/json',
// 				'Authorization' : `Bearer ${localStorage.getItem('token')}`
// 			},
// 			body: JSON.stringify({
// 				productId: `${productId}`
// 			})
// 		})
// 		.then(response => response.json())
// 		.then(data => {
// 			 // console.log(data);
// 			if(data === true){
// 				//successful yung login
// 				Swal2.fire({
// 					title: 'Checkout Successful!',
// 					icon: 'success',
// 					text: 'You have successfully checked out the product!'
// 				})
// 			}else{
// 				Swal2.fire({
// 					title: 'Checkout Unsuccessful',
// 					icon: 'error',
// 					text: 'Please try again!'
// 				})
// 			}
// 		})


// 	}


// 	return(
// 		<Row>
// 			<Col>
// 				<Card>
// 				    <Card.Body>
// 				      	<Card.Title>{name}</Card.Title>
// 				        <Card.Text>
// 				          {description}
// 				        </Card.Text>
// 				        <Card.Text>
// 				          Price: {price}
// 				        </Card.Text>
// 				        <Button variant="primary" onClick = {() => checkout(id)}>Checkout</Button>
// 				      </Card.Body>
// 				 </Card>
// 			</Col>
// 		</Row>
// 		)
// }

// -----------------------------------------------------------------------

import { Row, Col, Button, Card } from 'react-bootstrap';
import { useParams } from 'react-router-dom';
import { useEffect, useState } from 'react';
import Swal2 from 'sweetalert2';

export default function ProductView() {
  const [name, setName] = useState('');
  const [price, setPrice] = useState('');
  const [description, setDescription] = useState('');
  const [quantity, setQuantity] = useState(1);

  const { id } = useParams();

  useEffect(() => {
    const fetchProductDetails = () => {
      fetch(`${process.env.REACT_APP_API_URL}/products/${id}`)
        .then(response => response.json())
        .then(data => {
          if (data && data._id) {
            setName(data.name);
            setPrice(data.price);
            setDescription(data.description);
          } else {
            Swal2.fire({
              title: 'Product Not Found',
              icon: 'error',
              text: 'The product details could not be fetched.',
            });
          }
        })
        .catch(error => {
          console.error('Error fetching product details:', error);
          Swal2.fire({
            title: 'Error',
            icon: 'error',
            text: 'An error occurred while fetching product details.',
          });
        });
    };

    fetchProductDetails();
  }, [id]);

  const checkout = () => {
    fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({
        productId: id,
        quantity: quantity,
      }),
    })
      .then(response => response.json())
      .then(data => {
        if (data.success) {
          Swal2.fire({
            title: 'Checkout Successful!',
            icon: 'success',
            text: 'You have successfully checked out the product!',
          });
        } else {
          Swal2.fire({
            title: 'Checkout Unsuccessful',
            icon: 'error',
            text: 'Please try again!',
          });
        }
      })
      .catch(error => {
        console.error('Error during checkout:', error);
        Swal2.fire({
          title: 'Error',
          icon: 'error',
          text: 'An error occurred during checkout. Please try again later.',
        });
      });
  };

  return (
    <Row>
      <Col>
        <Card>
          <Card.Body>
            <Card.Title>{name}</Card.Title>
            <Card.Text>{description}</Card.Text>
            <Card.Text>Price: {price}</Card.Text>
            <input
              type="number"
              value={quantity}
              onChange={e => setQuantity(e.target.value)}
              min="1"
            />
            <Button variant="primary" onClick={checkout}>
              Checkout
            </Button>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}




