import { Form, Button, Row, Col } from "react-bootstrap";
// we need to import useState from react
import { useState, useEffect, useContext } from "react";
import { Navigate, useNavigate } from "react-router-dom";

import UserContext from "../UserContext";

import Swal2 from "sweetalert2";

export default function Register() {
  // State hooks to store the values of the input fields
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [mobileNo, setMobileNo] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isDisabled, setIsDisabled] = useState(true);

  const { user } = useContext(UserContext);
  const navigate = useNavigate();

  //   This useEffect will disable or enable our signup button
  useEffect(() => {
    // We are going to add if statement and all the condition that we mentioned should be satisfied before we enable the signup button
    if (
      email !== "" &&
      password !== "" &&
      firstName !== "" &&
      lastName !== "" &&
      mobileNo !== "" &&
      mobileNo.length === 11
    ) {
      setIsDisabled(false);
    } else {
      setIsDisabled(true);
    }
  }, [email, password, firstName, lastName, mobileNo]);

  // Funtion that will simulate user registration
  function registerUser(event) {
    event.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        email: email
      })
    })
      .then((response) => response.json())
      .then((data) => {
        if (data) {
          console.log(data);
          Swal2.fire({
            title: "Duplicate email found",
            icon: "info",
            text: "The email already exists"
          });
        } else {
          fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
            method: "POST",
            headers: {
              "Content-Type": "application/json"
            },
            body: JSON.stringify({
              email: email,
              password: password,
              firstName: firstName,
              lastName: lastName,
              mobileNo: mobileNo
            })
          })
            .then((response) => response.json())
            .then((data) => {
              console.log(data);
              if (data) {
                Swal2.fire({
                  title: "Registration Successful",
                  icon: "success",
                  text: "Thank you for registering"
                });

                navigate("/login");
                
              } else {
                Swal2.fire({
                  title: "Registration Failed",
                  icon: "error",
                  text: "Something went wrong, try again"
                });
              }
            });
        }
      });

    setEmail("");
    setPassword("");
    setFirstName("");
    setLastName("");
    setMobileNo("");
  }

  return user.id === null || user.id === undefined ? (
    <Row>
      <Col className="col-6 mx-auto">
        <h1 className="text-center">Register</h1>
        <Form onSubmit={(e) => registerUser(e)}>
          <Form.Group controlId="firstName" className="mb-3">
            <Form.Label>First Name</Form.Label>
            <Form.Control
              type="text"
              placeholder="First Name"
              className="mb-3"
              required
              value={firstName}
              onChange={(e) => setFirstName(e.target.value)}
            />
          </Form.Group>

          <Form.Group controlId="lastName" className="mb-3">
            <Form.Label>Last Name</Form.Label>
            <Form.Control
              type="text"
              placeholder="Last Name"
              required
              value={lastName}
              onChange={(e) => setLastName(e.target.value)}
            />
          </Form.Group>

          <Form.Group controlId="mobileNo" className="mb-3">
            <Form.Label>Mobile Number</Form.Label>
            <Form.Control
              type="text"
              placeholder="Mobile Number"
              required
              value={mobileNo}
              onChange={(e) => setMobileNo(e.target.value)}
            />
          </Form.Group>

          <Form.Group controlId="userEmail" className="mb-3">
            <Form.Label>Email Address</Form.Label>
            <Form.Control
              type="email"
              placeholder="Enter your email here"
              required
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
            <Form.Text className="text-muted">
              We'll never share your email with anyone else.
            </Form.Text>
          </Form.Group>

          <Form.Group controlId="password">
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Enter your password here"
              required
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </Form.Group>

          <Button variant="primary" type="submit" disabled={isDisabled}>
            Sign up
          </Button>
        </Form>
      </Col>
    </Row>
  ) : (
    <Navigate to="*" />
  );
}
